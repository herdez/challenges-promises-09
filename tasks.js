/*

The company 'jobtasks Co., Ltd.' has multiple asynchronous tasks to perform and 
they need to get a 'cipher' value when every task is done.

The software development team has received a POC (Proof of Concept) to resolve:


Description

There are four asynchronous tasks 'job1, job2, job3, job4' with the following
times, respectively:

- Job1 (1 seg).
- Job2 (2 seg).
- Job3 (0.5 seg).
- Job4 (1.5 seg).

Each 'job' has the responsability of returning a code:

- Job1 --> 10
- Job2 --> 30
- Job3 --> 70
- Job4 --> 35

When all asynchronous tasks are done we will get the following output:

```
[ { code: '10' }, { code: '30' }, { code: '70' }, { code: '35' } ]
```

It's necessary the usage of promises to reach a 'cipher' value.

Output example in terminal:

```
Resolving job3 with code 70, time: 0.5 seg.
Resolving job1 with code 10, time: 1 seg.
Resolving job4 with code 35, time: 1.5 seg.
Resolving job2 with code 30, time: 2 seg.
All done [ { code: '10' }, { code: '30' }, { code: '70' }, { code: '35' } ]
Cipher is... 10307035

```

*/


//+++ YOUR CODE GOES HERE


//job()







//promise







// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*



/* Output

Resolving job3 with code 70, time: 0.5 seg.
Resolving job1 with code 10, time: 1 seg.
Resolving job4 with code 35, time: 1.5 seg.
Resolving job2 with code 30, time: 2 seg.
All done [ { code: '10' }, { code: '30' }, { code: '70' }, { code: '35' } ]
Cipher is... 10307035

*/


